import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;

import java.io.IOException;
//////не закончен
public class MatchPhraseSlop {
  public static void main(String[] args) throws IOException {
    RestClient restClient = RestClient.builder(
      new HttpHost("localhost", 9200)).build();


    ElasticsearchTransport transport = new RestClientTransport(
      restClient, new JacksonJsonpMapper());


    ElasticsearchClient client = new ElasticsearchClient(transport);


    var response = client.search(search -> search.index("employees")
        .query(query -> query.bool(
            bool -> bool
              .must(must -> must.match(match -> match.field("match_phrase").field("phrase").query("roots coherent")))
          )
        ),
      Employees.class);


    for (Hit<Employees> hit : response.hits().hits()) {
      assert hit.source() != null;
      processEmployees(hit.source());
    }
  }

  private static void processEmployees(Employees source) {
    System.out.println(source.toString());
  }

}

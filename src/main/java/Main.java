import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;

import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {

    RestClient restClient = RestClient.builder(
      new HttpHost("localhost", 9200)).build();

// Create the transport with a Jackson mapper
    ElasticsearchTransport transport = new RestClientTransport(
      restClient, new JacksonJsonpMapper());

// And create the API client
    ElasticsearchClient client = new ElasticsearchClient(transport);


    SearchResponse<Employees> search = client.search(s -> s
        .index("employees")
        .query(q -> q
          .term(t -> t
            .field("experience")
            .value(7)
          )),
      Employees.class);
    for (Hit<Employees> hit : search.hits().hits()) {
      assert hit.source() != null;
      processEmployees(hit.source());

    }

  }


  private static void processEmployees(Employees source) {
    System.out.println(source.toString());
  }


}


import java.util.Date;

public class Employees {
    private int id;
    private String name;
    private String email;
    private String gender;
    private String ip_address;
    private String date_of_birth;
    private String company;
    private String position;
    private String experience;
    private String country;
    private String phrase;
    private int salary;

    public Employees(int id, String name, String email, String gender, String ip_address, String date_of_birth, String company, String position, String experience, String country, String phrase, int salary) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.ip_address = ip_address;
        this.date_of_birth = date_of_birth;
        this.company = company;
        this.position = position;
        this.experience = experience;
        this.country = country;
        this.phrase = phrase;
        this.salary = salary;
    }

    public Employees() {
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", ip_address='" + ip_address + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                ", company='" + company + '\'' +
                ", position='" + position + '\'' +
                ", experience='" + experience + '\'' +
                ", country='" + country + '\'' +
                ", phrase='" + phrase + '\'' +
                ", salary=" + salary +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}

